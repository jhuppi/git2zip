from setuptools import setup

setup(
    name='git2zip',
    version='0.2.0',
    py_modules=['git2zip'],
    install_requires=[
        'Click',
    ],
    entry_points={
        'console_scripts': {
            'git2zip = git2zip:cli'
        },
    },
)
