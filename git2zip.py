import os
from pathlib import Path
import shutil
import subprocess

import click


@click.command()
@click.argument('src', type=click.Path(exists=True))
@click.argument('dst', type=click.Path(exists=True))
@click.option('-z', '--zip', type=click.Path(file_okay=True))
def cli(src, dst, zip):
    src_abs = os.path.abspath(src)
    dst_abs = os.path.abspath(dst)
    if zip:
        zip_abs = os.path.abspath(zip)

    # enter git repo
    os.chdir(src_abs)

    # retrieve most recent commit
    commit = subprocess.check_output(
        ['git', 'diff-tree', '--no-commit-id', '--name-status', '-r', 'HEAD'],
        encoding='UTF-8')

    # split comitted files
    files = commit.strip().split('\n')

    # separate status and path
    paths = []
    for file in files:
        status, path = file.split('\t')

        if status != 'D':
            # separate dir and base names
            paths.append((os.path.dirname(path), os.path.basename(path)))

    # confirm files to be copied
    click.echo('parent directory:')
    click.echo(click.style(dst, fg='yellow'))

    for path in paths:
        dir, base = path
        dir = dir if dir else '.'

        click.echo(click.style(base, fg='cyan'), nl=False)
        click.echo(' to be copied to:')
        click.echo(click.style(dir, fg='green'))

    if not click.confirm('continue?'):
        return

    # move to distribution directory
    os.chdir(dst_abs)
    with click.progressbar(paths, label='copying') as bar:
        for path in bar:
            dir, base = path
            dir = dir if dir else '.'

            # create directory structure at dst
            os.makedirs(dir, exist_ok=True)
            # copy files into new directory structure
            shutil.copy(Path(src_abs)/dir/base, dir)

    # compress the distribution directory
    if zip:
        shutil.make_archive(zip_abs, 'zip', dst_abs)
    else:
        shutil.make_archive(dst_abs, 'zip', dst_abs)
